import os
import shutil
import re

# Define new directory structure
new_structure = {
    'images/icons': ['favicon.ico', 'favicon-16x16.png', 'favicon-32x32.png', 'apple-touch-icon.png', 'android-chrome-192x192.png', 'android-chrome-512x512.png'],
    'images/backgrounds': ['header.png', 'grass.png', 'icky-paintlike-goop.jpg'],
    'images/misc': ['arch.gif', 'live-indicator.gif', 'neocities.png', 'stars3.gif', 'pyro.gif', 'site-button.gif'],
    'css': ['.css'],
    'js': ['.js'],
    'fonts': ['.ttf'],
    'pdfs': ['.pdf'],
    'audio': ['.mp3'],
    'video': ['.mp4']
}

# Create directories if they do not exist
for folder in new_structure:
    folder_path = os.path.join('assets', folder)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

# Move files to their respective directories
for file in os.listdir('.'):
    if file == 'organize.py' or file in ['LICENSE', 'README.md']:
        continue  # Skip the script itself and non-relevant files
    file_ext = os.path.splitext(file)[1].lower()
    moved = False
    for folder, extensions in new_structure.items():
        if file in extensions or file_ext in extensions:
            if os.path.isfile(file):
                shutil.move(file, os.path.join('assets', folder, file))
                moved = True
            break
    if not moved and file_ext in ['.png', '.jpg', '.gif', '.ico']:
        if os.path.isfile(file):
            shutil.move(file, os.path.join('assets', 'images/misc', file))

# Function to update file paths in code files
def update_paths(file_path):
    if not os.path.isfile(file_path):
        return
    with open(file_path, 'r') as file:
        content = file.read()

    for folder, extensions in new_structure.items():
        for ext in extensions:
            if ext.startswith('.'):
                content = re.sub(r'(["\'\(])(\./)?([^/]+{})\b'.format(ext), r'\1assets/{}/\3'.format(folder), content)
            else:
                content = re.sub(r'(["\'\(])(\./)?{}["\']'.format(ext), r'\1assets/{}/{}'.format(folder, ext), content)

    with open(file_path, 'w') as file:
        file.write(content)

# List of code files to update
code_files = ['index.html', 'breadbug.html', 'style.css', 'script.js']

for code_file in code_files:
    update_paths(code_file)

print("Files organized and paths updated successfully.")

