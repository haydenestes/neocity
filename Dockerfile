FROM python:3.10-slim        # Base image
WORKDIR /app                 # Set working directory
COPY . /app                  # Copy files to container
EXPOSE 8000                  # Expose port
CMD ["python", "webhost.py"] # Command to run the server

